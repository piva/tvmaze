package com.pierrevanderpol.tvmaze.search

import com.nhaarman.mockitokotlin2.verifyBlocking
import com.nhaarman.mockitokotlin2.whenever
import com.pierrevanderpol.tvmaze.data.TVShowRepository
import com.pierrevanderpol.tvmaze.model.Show
import com.pierrevanderpol.tvmaze.network.Result
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.MISSING_TEXT_ERROR
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.NO_DATA_FOUND
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.REQUEST_ERROR
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchPresenterTest {

    @Mock
    private lateinit var mockView: SearchContract.View
    @Mock
    private lateinit var mockRepository: TVShowRepository
    private lateinit var searchPresenter: SearchPresenter
    private lateinit var shows: List<Show>
    private lateinit var expected: Result<List<Show>>


    @Before
    fun setUp() {
        /**
        * Necessary to avoid Exception in thread "main" java.lang.IllegalStateException: Module with the Main dispatcher had failed to initialize.
        * */
        @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
        Dispatchers.setMain(Dispatchers.Unconfined)

        searchPresenter = SearchPresenter(
            view = mockView,
            showRepository = mockRepository
        )

        shows = listOf(
            Show(id = 139, url = "http://www.tvmaze.com/shows/139/girls", name = "Girls"),
            Show(id = 33320, url = "http://www.tvmaze.com/shows/33320/derry-girls", name = "Derry Girls"),
            Show(id = 6771, url = "http://www.tvmaze.com/shows/6771/the-powerpuff-girls", name = "The Powerpuff Girls")
        )

        expected = Result.Success(shows)

    }

    /**
     * Necessary to avoid Exception in thread "main" java.lang.IllegalStateException: Module with the Main dispatcher had failed to initialize.
     * */
    @After
    fun tearDown() {
        @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun empty_search_displays_missing_text_Error() = runBlockingTest {
        //Invoke
        searchPresenter.search("")
        //Assert
        verify(mockView).displayError(MISSING_TEXT_ERROR)
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun launching_search_displays_loading() = runBlockingTest {
        //Invoke
        searchPresenter.search("anything")
        //Assert
        verify(mockView).displayLoading()
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun searching_show_displays_list_of_shows_to_user() = runBlockingTest {
        CoroutineScope(Dispatchers.Unconfined).launch {
            whenever(mockRepository.getSearchResult(ArgumentMatchers.anyString())).thenReturn(
                expected
            )
        }
        //Invoke
        searchPresenter.searchShows("girls")
        //Assert
        verifyBlocking(mockView) {
            val argumentCaptor = argumentCaptor<List<Show>>()
            displayResults(capture(argumentCaptor))
            displayResults(shows)
            assertTrue(argumentCaptor.value.size == 3)
        }
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun show_not_found_displays_not_found_error_to_user() = runBlockingTest {
        CoroutineScope(Dispatchers.Unconfined).launch {
            whenever(mockRepository.getSearchResult(ArgumentMatchers.anyString())).thenReturn(
                Result.NoDataFound
            )

        }
        //Invoke
        searchPresenter.searchShows("/*/")
        //Assert
        verifyBlocking(mockView) {
            displayError(NO_DATA_FOUND)
        }
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun error_from_api_displays_request_error_to_user() = runBlockingTest {
        CoroutineScope(Dispatchers.Unconfined).launch {
            whenever(mockRepository.getSearchResult(ArgumentMatchers.anyString())).thenReturn(
                Result.ResponseException
            )
        }
        //Invoke
        searchPresenter.searchShows("kaboom")
        //Assert
        verifyBlocking(mockView) {
            displayError(REQUEST_ERROR)
        }
    }

    @UseExperimental(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    @Test
    fun connection_loss_displays_internet_connection_error_to_user() = runBlockingTest {
        CoroutineScope(Dispatchers.Unconfined).launch {
            whenever(mockRepository.getSearchResult(ArgumentMatchers.anyString())).thenReturn(
                Result.InternetConnexionException
            )
        }
        //Invoke
        searchPresenter.searchShows("kaboom")
        //Assert
        verifyBlocking(mockView) {
            displayError(SearchActivity.NO_INTERNET_ERROR)
        }
    }


    /**
     * Helper function for creating an argumentCaptor in kotlin.
     * @see {@linktourl https://github.com/dmytrodanylyk/android-architecture }
     * @author Dmytro Danylyk
     */
    private inline fun <reified T : Any> argumentCaptor(): ArgumentCaptor<T> = ArgumentCaptor.forClass(T::class.java)

    /**
     * Returns ArgumentCaptor.capture() as nullable type to avoid java.lang.IllegalStateException
     * when null is returned.
     * @see {@linktourl https://github.com/dmytrodanylyk/android-architecture }
     * @author Dmytro Danylyk
     */
    private fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

}


