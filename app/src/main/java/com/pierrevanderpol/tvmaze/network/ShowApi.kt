package com.pierrevanderpol.tvmaze.network

import com.pierrevanderpol.tvmaze.model.SearchResult
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ShowApi {
    @GET("search/shows")
    fun searchShowsAsync(@Query("q") q: String): Deferred<Response<List<SearchResult>>>
}