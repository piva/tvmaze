package com.pierrevanderpol.tvmaze.network



sealed class Result<out T: Any> {
    class Success<out T : Any>(val data: T): Result<T>()
    object NoDataFound : Result<Nothing>()
    object InternetConnexionException : Result<Nothing>()
    object ResponseException : Result<Nothing>()
}