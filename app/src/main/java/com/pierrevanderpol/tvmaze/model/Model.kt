package com.pierrevanderpol.tvmaze.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchResult(
    @Expose
    var score: Float? = null,
    @Expose
    var show: Show? = null
): Parcelable

@Parcelize
data class Show(
    @Expose
    var id: Int? = null,
    @Expose
    var url: String? = null,
    @Expose
    var name: String? = null,
    @Expose
    var type: String? = null,
    @Expose
    var language: String? = null,
    @Expose
    var genres: List<String>? = null,
    @Expose
    var status: String? = null,
    @Expose
    var runtime: Int? = null,
    @Expose
    var premiered: String? = null,
    @Expose
    var officialSite: String? = null,
    @Expose
    var schedule: Schedule? = null,
    @Expose
    var rating: Average? = null,
    @Expose
    var weight: Int? = null,
    @Expose
    var network: Network? = null,
    @Expose
    var externals: Externals? = null,
    @Expose
    var image: Image? = null,
    @Expose
    var summary: String? = null,
    @Expose
    var updated: Int? = null,
    @SerializedName("_links")
    @Expose
    var links: Links? = null
): Parcelable

@Parcelize
data class Schedule(
    @Expose
    var time: String? = null,
    @Expose
    var days: List<String>? = null
): Parcelable

@Parcelize
data class Average(
    @Expose
    var average: Float? = null
): Parcelable

@Parcelize
data class Network(
    @Expose
    var id: Int? = null,
    @Expose
    var name: String? = null,
    @Expose
    var country: Country? = null
): Parcelable

@Parcelize
data class Country(
    @Expose
    var name: String? = null,
    @Expose
    var code: String? = null,
    @Expose
    var timezone: String? = null
): Parcelable

@Parcelize
data class Externals(
    @Expose
    var tvrage: String? = null,
    @Expose
    var thetvdb: String? = null,
    @Expose
    var imdb: String? = null
): Parcelable

@Parcelize
data class Image(
    @Expose
    var medium: String? = null,
    @Expose
    var original: String? = null
): Parcelable

@Parcelize
data class Links(
    @Expose
    var self: Self? = null,
    @Expose
    var previousepisode: PreviousEpisode? = null
): Parcelable

@Parcelize
data class Self(
    @Expose
    var href: String? = null
): Parcelable

@Parcelize
data class PreviousEpisode(
    @Expose
    var href: String? = null
): Parcelable
