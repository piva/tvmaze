package com.pierrevanderpol.tvmaze.search

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.pierrevanderpol.tvmaze.R
import com.pierrevanderpol.tvmaze.data.RemoteDataSource
import com.pierrevanderpol.tvmaze.detail.DetailActivity
import com.pierrevanderpol.tvmaze.search.adapter.ShowAdapter
import com.pierrevanderpol.tvmaze.model.Show
import kotlinx.android.synthetic.main.search_main.*

class SearchActivity: AppCompatActivity(), SearchContract.View {

    private lateinit var adapter: ShowAdapter
    private lateinit var progressBar: ProgressBar
    private var results: MutableList<Show> = mutableListOf()
    private lateinit var searchPresenter: SearchContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpPresenter()
        setUpViews(savedInstanceState)
    }

    private fun setUpPresenter(){
        searchPresenter = SearchPresenter(
            this,
            RemoteDataSource()
        )
    }

    private fun setUpViews(savedInstanceState: Bundle?){
        setContentView(R.layout.search_main)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ShowAdapter(listOf(), this@SearchActivity, itemListener)
        recyclerView.adapter = adapter
        progressBar = findViewById(R.id.progressBar)
        searchBtn.setOnClickListener {

            searchPresenter.search(searchText.text.toString())

        }

        savedInstanceState?.run {
            val bundle = savedInstanceState.getBundle(BUNDLED_SHOWS)
            val savedShows = bundle.getParcelableArrayList<Show>(LIST)
            adapter.showList = savedShows
            results.addAll(savedShows)
            recyclerView.visibility = VISIBLE
            adapter.notifyDataSetChanged()
        }
    }

    override fun displayResults(resultList: List<Show>) {
        progressBar.visibility = INVISIBLE
        recyclerView.visibility = VISIBLE
        adapter.showList = resultList
        results.clear()
        results.addAll(resultList)
        adapter.notifyDataSetChanged()
    }

    override fun displayLoading() {
        progressBar.visibility = VISIBLE
        recyclerView.visibility = INVISIBLE
        infoText.visibility = INVISIBLE
    }

    override fun displayError(errorConst: String) {
        progressBar.visibility = INVISIBLE
        recyclerView.visibility = INVISIBLE
        infoText.visibility = VISIBLE
        val errorMsg = when(errorConst){
            MISSING_TEXT_ERROR -> getString(R.string.user_prompt_enter_title)
            NO_INTERNET_ERROR -> getString(R.string.check_internet_connection)
            NO_DATA_FOUND -> getString(R.string.no_result_found)
            else -> getString(R.string.error_retry_later)
        }
        infoText.text = errorMsg
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        val bundle = Bundle().apply {
            putParcelableArrayList(LIST, ArrayList<Parcelable>(results))
        }
        outState?.putParcelable(BUNDLED_SHOWS, bundle)
    }

    private var itemListener: RecyclerItemListener = object :
        RecyclerItemListener {
        override fun onItemClick(view: View, position: Int) {
            val movie = adapter.getItemAtPosition(position)
            val replyIntent = Intent(this@SearchActivity, DetailActivity::class.java)
            replyIntent.putExtra(SELECTED_SHOW, movie)
            startActivity(replyIntent)
        }
    }

    interface RecyclerItemListener {
        fun onItemClick(view: View, position: Int)
    }

    companion object {
        const val SELECTED_SHOW = "selectedShow"
        const val BUNDLED_SHOWS = "BundledShows"
        const val LIST = "list"
        const val MISSING_TEXT_ERROR = "noTitle"
        const val NO_INTERNET_ERROR = "noInternet"
        const val REQUEST_ERROR = "requestError"
        const val NO_DATA_FOUND = "noResult"
    }

    override fun onStop() {
        super.onStop()
        searchPresenter.clear()
    }
}
