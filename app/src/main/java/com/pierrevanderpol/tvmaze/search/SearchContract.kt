package com.pierrevanderpol.tvmaze.search

import com.pierrevanderpol.tvmaze.model.Show

interface SearchContract {

    interface Presenter {
        fun search(query: String)
        fun clear()
    }

    interface View {
        fun displayResults(resultList: List<Show>)
        fun displayLoading()
        fun displayError(s: String)
    }

}