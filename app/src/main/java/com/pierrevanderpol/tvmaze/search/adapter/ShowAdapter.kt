package com.pierrevanderpol.tvmaze.search.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pierrevanderpol.tvmaze.search.SearchActivity
import com.pierrevanderpol.tvmaze.R
import com.pierrevanderpol.tvmaze.model.Show
import com.squareup.picasso.Picasso

class ShowAdapter(internal var showList: List<Show>, private var context: Context, var listener: SearchActivity.RecyclerItemListener) :
    RecyclerView.Adapter<ShowAdapter.ShowHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_show, parent, false)
        val viewHolder = ShowHolder(view)
        view.setOnClickListener { v -> listener.onItemClick(v, viewHolder.adapterPosition) }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return showList.size
    }

    override fun onBindViewHolder(holder: ShowHolder, position: Int) {
        holder.titleTextView.text = showList[position].name
        holder.tvShowImageView.setImageDrawable(context.getDrawable(R.drawable.ic_no_image))
        if (!showList[position].image?.medium.isNullOrEmpty()) {
            Picasso.get().load(showList[position].image?.medium).into(holder.tvShowImageView)
        }
    }

    fun getItemAtPosition(position: Int): Show {
        return showList[position]
    }

    inner class ShowHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var tvShowImageView: ImageView = v.findViewById(R.id.movie_iv)
        internal var titleTextView: TextView = v.findViewById(R.id.title_tv)

        init {
            v.setOnClickListener { v: View -> listener.onItemClick(v, adapterPosition) }
        }
    }


}
