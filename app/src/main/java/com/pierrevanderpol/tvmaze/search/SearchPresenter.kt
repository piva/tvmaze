package com.pierrevanderpol.tvmaze.search

import com.pierrevanderpol.tvmaze.data.TVShowRepository
import com.pierrevanderpol.tvmaze.network.Result
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.MISSING_TEXT_ERROR
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.NO_INTERNET_ERROR
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.NO_DATA_FOUND
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.REQUEST_ERROR
import kotlinx.coroutines.*

class SearchPresenter(
    private val view: SearchContract.View,
    private val showRepository: TVShowRepository
) : SearchContract.Presenter {

    private val job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)


    override fun search(title: String) {
        if (title.isEmpty()) {
            view.displayError(MISSING_TEXT_ERROR)
        } else {
            searchShows(title)
            view.displayLoading()
        }
    }

    fun searchShows(title: String) {
        uiScope.launch {
            try {
                when (val result = withContext(Dispatchers.IO){
                    showRepository.getSearchResult(title)
                }) {
                    is Result.Success -> view.displayResults(result.data)
                    is Result.NoDataFound -> view.displayError(NO_DATA_FOUND)
                    is Result.ResponseException -> view.displayError(REQUEST_ERROR)
                    else -> view.displayError(NO_INTERNET_ERROR)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                view.displayError(NO_INTERNET_ERROR)
            }

        }
    }

    // Cancel Coroutine job when the view is destroyed
    override fun clear() {
        job.cancel()
    }


}
