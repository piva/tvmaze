package com.pierrevanderpol.tvmaze.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pierrevanderpol.tvmaze.search.SearchActivity.Companion.SELECTED_SHOW
import com.pierrevanderpol.tvmaze.R
import com.pierrevanderpol.tvmaze.model.Show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*

@Suppress("RegExpRedundantEscape")
class DetailActivity : AppCompatActivity() {
    private lateinit var show: Show

    @Suppress("RegExpRedundantEscape")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)

        show = intent.getParcelableExtra(SELECTED_SHOW)

        title_tv.text = show.name

        if (!show.image?.medium.isNullOrEmpty()) {
            Picasso.get().load(show.image?.medium).into(movie_iv)
        }
        //Cleans HTML chars in the summary
        val pattern = "\\<.*?\\>".toRegex()
        summary_tv.text = show.summary?.replace(pattern,"")
        show_premiered_tv.text = show.premiered

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(SELECTED_SHOW, show)
    }

}