package com.pierrevanderpol.tvmaze.data

import com.pierrevanderpol.tvmaze.model.Show
import com.pierrevanderpol.tvmaze.network.Result
import com.pierrevanderpol.tvmaze.network.RetrofitClient
import kotlinx.coroutines.withTimeoutOrNull
import org.json.JSONObject

const val TIMEOUT_DURATION_MILLIS = 5000L

open class RemoteDataSource : TVShowRepository {

    override suspend fun getSearchResult(title: String): Result<List<Show>> {

        val response = withTimeoutOrNull(TIMEOUT_DURATION_MILLIS) {
            RetrofitClient.showApi.searchShowsAsync(title).await()
        }

        if (response != null) {
            if (response.isSuccessful) {
                val results: MutableList<Show> = mutableListOf()
                return if (response.body().isNullOrEmpty() || response.body()?.size == 0) {
                    Result.NoDataFound
                } else {
                    response.body()?.forEach {
                        it.show?.let { show ->
                            results.add(show)
                           //d("show", show.toString()) //generates error in the UnitTests
                        }
                    }
                    Result.Success(results.toList())
                }
            } else {
                try {
                    val responseErrorBody = JSONObject(response.errorBody()!!.string())
                    //Log.e("searchShow error: ", responseErrorBody.getString("error")) //generates error in the UnitTests
                } catch (e: Exception) {
                    //Log.e("searchShow exception", e.message) //generates error in the UnitTests
                }
                return Result.ResponseException
            }
        } else {
           // Log.e("error", "connection")
            return Result.InternetConnexionException
        }


    }
}
