package com.pierrevanderpol.tvmaze.data

import com.pierrevanderpol.tvmaze.model.Show
import com.pierrevanderpol.tvmaze.network.Result

interface TVShowRepository{
    suspend fun getSearchResult(s: String): Result<List<Show>>
}
