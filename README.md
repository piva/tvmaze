# tvmaze

Android Developer coding test.
@Author Pierre Vanderpol
Update : 219/06/24

Demo app consuming REST API http://www.tvmaze.com/api#show-search

Requirements/Scope :
• Search for shows
• Parse the response into data models that you use to render a list of items.
• Show a loading indicator while the request is executing.
• Each list cell should display an image and title from each model.
• If the request fails you should show an error message and give the user a way of retrying the request.
• When the user clicks on a list item, show a detailed view of the show 
• The application needs to run on lollipop (and newer 21+) devices.
• The application works in all orientations but requires no special ui for landscape/tablet.
• git repository service
• Write a README in the git repo where you motivate code design decisions and library
usage.

Choices:
To reduce boiler plate, increase readability and optimize coding time, I use common dependencies, updated, well documented and well-known by Android
developers.

• Kotlin Annotation processor
• Layout: AndroidX compat library (fully replaces Support Library)
• Network: Okhttp / Retrofit V2
• JSON handling: convert response to data classes with GSON (other choices are possible. moshi or Jackson)
• Handling asynchronous operations with Kotlin coroutines (instead of RXJava, more heavy to use on a simple project)
• Diplay data fetched in the UI: RecyclerView (more efficient than list)
• Image loading handler: Picasso (second choice : Glide)
• Code repo : GitLab since I can have for free private / public repo with CI / CD pipeline (YML file is added for continuous integration on a GitLab repo with Lint Inspection for quality check)
• Kotlin Parcelable to recover parcelable objects in the Bundle (on configuration change)

Code choice:
One activity to display the following UI components:
• search box with button
• the result list
• loading symbol
• Error messages
• ClickListener to detect click on RecyclerView items.
• MainView as MVP architecture patterns with interface and Presenter

• Another activity is used to display Show details (no MVP because the view only displays data. No interaction, no logic to test).
• Data on rotation is saved in the bundle and restored in both activities.
• HTML tags removed from data displayed in detail view
• Retry strategy / timeout for Networking: coroutine time out after 5 seconds and user message "Check internet connection " is presented to the user

Code Quality: 
• Build on CI GitLab is LINT and Unit-test validated, otherwise build is failed.

Limitations / future evolutions for a version 2.0:
• UI test / Instrumentation test not yet implemented
• More polished design with custom fonts and animations
• Tablet layout
